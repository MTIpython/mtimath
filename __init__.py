r"""
.. module:: MTIpython.mtimath
    :platform: Unix, Windows
    :synopsis: packages with universal math functions

.. moduleauthor:: Jelle Spijker <j.spijker@ihcmti.com>
"""

from MTIpython.mtimath import geometry, types

__all__ = ['types', 'geometry']

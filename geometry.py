r"""
.. module:: MTIpython.mtimath.geometry
    :platform: Unix, Windows
    :synopsis: packages with math functions, regarding geometry

.. moduleauthor:: Jelle Spijker <j.spijker@ihcmti.com>
"""
from warnings import warn
from MTIpython.core.functions import FunctionBundle
from MTIpython.mtimath.types import pi

__all__ = ['A_circle', 'surface', 'diameter', 'radius', 'delta_d']


def A_circle(r=None, D=None):
    r"""
    Surface area of a circle

    Args:
        r (Quantity, float, int): Describing the radius in :math:`[m]`
        D (Quantity, float, int): Describing the radius in :math:`[m]`

    Returns:
        Surface (:class:`~pint:.Quantity` or :class:`.Binder`): in :math:`[m^2]`

    .. deprecated:: 0.2
       Use :class:`~.surface`

    """
    warn("A_circle is deprecated, use surface instead!", DeprecationWarning, stacklevel=2)
    if r is None and D is not None:
        A = (pi / 4) * D.to('m') ** 2
    else:
        A = pi * r.to('m') ** 2
    return A


class surface(FunctionBundle):
    r"""
    Calculates the surface.

    Returns:
        surface :math:`A` in :math:`[L^{2}]`
    """

    @staticmethod
    def from_diameter_disc(D):
        r"""
        Calculate the surface :math:`A` in :math:`[L^{2}]` of a disc using a given diameter

        .. math::

           A = \frac{\pi}{4} D^2

        Args:
            D: diameter :math:`D` in :math:`[L^{1}]`

        Returns:
            surface :math:`A` in :math:`[L^{2}]`
        """
        return (pi / 4) * D ** 2

    @staticmethod
    def from_radius_disc(r):
        r"""
        Calculate the surface :math:`A` in :math:`[L^{2}]` of a disc using a given radius

        .. math::

           A = \pi r^2

        Args:
            r: radius :math:`r` in :math:`[L^{1}]`

        Returns:
            surface :math:`A` in :math:`[L^{2}]`
        """
        return pi * r ** 2

    @staticmethod
    def from_radius_hollow_disc(r_i, r_o):
        r"""
        Calculate the surface :math:`A` in :math:`[L^{2}]` of a hollow disc using a given outer and inner radius

        .. math::

           A = \pi (r_o^2 - r_i^2)

        Args:
            r_i: inner radius :math:`r` in :math:`[L^{1}]`
            r_o: outer radius :math:`r` in :math:`[L^{1}]`

        Returns:
            surface :math:`A` in :math:`[L^{2}]`
        """
        return pi * (r_o ** 2 - r_i ** 2)

    @staticmethod
    def from_diameters_hollow_disc(d_i, d_o):
        r"""
        Calculate the surface :math:`A` in :math:`[L^{2}]` of a hollow disc using a given outer and inner diameter

        .. math::

           A = \frac{\pi}{4} * (d_o^2 - d_i^2)

        Args:
            d_i: inner diameter :math:`D` in :math:`[L^{1}]`
            d_o: outer diameter :math:`D` in :math:`[L^{1}]`

        Returns:
            surface :math:`A` in :math:`[L^{2}]`
        """
        return pi / 4 * (d_o ** 2 - d_i ** 2)

    @staticmethod
    def from_height_weight(h, w):
        r"""
        Calculate the surface :math:`A` in :math:`[L^{2}]` of a rectangular plate using a given height and weight

        .. math::

           A = h w

        Args:
            h: height :math:`h` in :math:`[L^{1}]`
            w: width :math:`w` in :math:`[L^{1}]`

        Returns:
            surface :math:`A` in :math:`[L^{2}]`
        """
        return h * w


class diameter(FunctionBundle):
    r"""
    Calculates the diameter

    Returns:
        diameter :math:`D` in :math:`[L^{1}]`
    """

    @staticmethod
    def from_surface(A):
        r"""
        Calculate the diameter :math:`D` in :math:`[L^{1}]` for a disc with a known surface

        .. math::

           D = \sqrt{\frac{4 A}{\pi}}

        Args:
            A: surface :math:`A` in :math:`[L^{2}]`

        Returns:
            diameter :math:`D` in :math:`[L^{1}]`
        """
        return ((4 * A) / pi) ** (1 / 2)

    @staticmethod
    def from_radius(r):
        r"""
        Calculate the diameter :math:`D` in :math:`[L^{1}]` from a radius

        .. math::

           D = 2 r

        Args:
            r: radius :math:`r` in :math:`[L^{1}]`

        Returns:
            diameter :math:`D` in :math:`[L^{1}]`
        """
        return r * 2


class radius(FunctionBundle):
    r"""
    Calculates the radius

    Returns:
        radius :math:`r` in :math:`[L^{1}]`
    """

    @staticmethod
    def from_surface(A):
        r"""
        Calculate the radius :math:`r` in :math:`[L^{1}]` for a disc with a known surface

        .. math::

           r = \sqrt{\frac{A}{\pi}}

        Args:
            A: surface :math:`A` in :math:`[L^{2}]`

        Returns:
            radius :math:`r` in :math:`[L^{1}]`
        """
        return (A / pi) ** (1 / 2)

    @staticmethod
    def from_diameter(D):
        r"""
        Calculate the radius :math:`r` in :math:`[L^{1}]` from a diameter

        .. math::

           r = \frac{D}{2}

        Args:
            D: diameter :math:`D` in :math:`[L^{1}]`

        Returns:
            radius :math:`r` in :math:`[L^{1}]`
        """
        return D / 2


class delta_d(FunctionBundle):
    r"""
    Calculate the difference in distance

    Returns:
        distance :math:`\Delta d` in :math:`[L^{1}]`
    """

    @staticmethod
    def from_diameters(d_i, d_o):
        r"""
        Calculate the distance :math:`\Delta d` in :math:`[L^{1}]` between between two circle, with coinciding center
        points using the inner and outer diameter.

        .. math::

           \Delta d = \frac{d_o - d_i}{2}

        Args:
            d_i: inner diameter :math:`D` in :math:`[L^{1}]`
            d_o: outer diameter :math:`D` in :math:`[L^{1}]`

        Returns:
            distance :math:`\Delta d` in :math:`[L^{1}]`
        """
        return (d_o - d_i) / 2

    @staticmethod
    def from_radius(r_i, r_o):
        r"""
        Calculate the distance :math:`\Delta d` in :math:`[L^{1}]` between between two circle, with coinciding center
        points using the inner en outer radius.

        .. math::

           \Delta d = r_o - r_i

        Args:
            r_i: inner radius :math:`r` in :math:`[L^{1}]`
            r_o: outer radius :math:`r` in :math:`[L^{1}]`

        Returns:
            distance :math:`\Delta d` in :math:`[L^{1}]`
        """
        return r_o - r_i

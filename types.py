r"""
.. module:: MTIpython.mtimath.types
    :platform: Unix, Windows
    :synopsis: packages with universal math types

.. moduleauthor:: Jelle Spijker <j.spijker@ihcmti.com>

.. py:data:: pi

   The mathematical constant :math:`\pi = 3.141592...`, to available precision.

.. py:data:: nan

   A floating-point “not a number” (NaN) value. Equivalent to the output of float('nan').

.. py:data:: inf

   A floating-point positive infinity :math:`\infty`. (For negative infinity, use -math.inf.) Equivalent to the
   output of float('inf').

"""
from math import inf, nan, pi

__all__ = ['pi', 'nan', 'inf']
